package ifaith

type FBaseView struct {
	FBase
}
type FBase struct {
	VID, ClassName string
	UI             string
}
type IView interface {
	GetViewId() string
}

func (v FBase) GetViewId() string {
	return v.VID
}

// ===================================
func (v *FBaseView) Show() {
	GlobalVars.UIs[v.UI].Stick2RootView(v.VID)
}

func (v *FBaseView) Size(w, h int) {
	GlobalVars.UIs[v.UI].ViewSetAttr(v.VID, "Size", SPrintf(w, ",", h))
}
func (v *FBaseView) BackgroundColor(s string) {
	GlobalVars.UIs[v.UI].ViewSetAttr(v.VID, "BackgroundColor", s)
}

func (v *FBaseView) Gravity(gravity string) {
	GlobalVars.UIs[v.UI].ViewSetAttr(v.VID, "Gravity", gravity)
}
