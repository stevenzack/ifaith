package ifaith

type FButton struct {
	FBaseView
}

func Button(a IActivity) *FButton {
	v := &FButton{}
	v.VID = NewToken()
	v.ClassName = "Button"
	v.UI = a.GetMyActivity().UI
	GlobalVars.UIs[v.UI].NewView(v.ClassName, v.VID)
	GlobalVars.ViewMap[v.VID] = v
	return v
}

// ----------------------------
func (v *FButton) Size(w, h int) *FButton {
	v.FBaseView.Size(w, h)
	return v
}
func (v *FButton) BackgroundColor(s string) *FButton {
	v.FBaseView.BackgroundColor(s)
	return v
}
func (v *FButton) Gravity(s string) *FButton {
	v.FBaseView.Gravity(s)
	return v
}

// --------------------------------------------------------
func (v *FButton) Text(s string) *FButton {
	GlobalVars.UIs[v.UI].ViewSetAttr(v.VID, "Text", s)
	return v
}
func (v *FButton) TextColor(s string) *FButton {
	GlobalVars.UIs[v.UI].ViewSetAttr(v.VID, "TextColor", s)
	return v
}
func (v *FButton) TextSize(dpsize int) *FButton {
	GlobalVars.UIs[v.UI].ViewSetAttr(v.VID, "TextSize", SPrintf(dpsize))
	return v
}
func (v *FButton) GetText() string {
	return GlobalVars.UIs[v.UI].ViewGetAttr(v.VID, "Text")
}
func (v *FButton) Enabled(b bool) *FButton {
	if b {
		GlobalVars.UIs[v.UI].ViewSetAttr(v.VID, "Enabled", "true")
	} else {
		GlobalVars.UIs[v.UI].ViewSetAttr(v.VID, "Enabled", "false")
	}
	return v
}
func (v *FButton) IsEnabled() bool {
	if GlobalVars.UIs[v.UI].ViewGetAttr(v.VID, "Enabled") == "true" {
		return true
	}
	return false
}
