package ifaith

var GlobalVars = struct {
	IdMap            map[string]IView //only store views that user set special Id on
	ViewMap          map[string]IView //store every view
	EventHandlersMap map[string]func(string) string
	UIs              map[string]UIBridgeInterface
}{
	make(map[string]IView),
	make(map[string]IView),
	make(map[string]func(string) string),
	make(map[string]UIBridgeInterface),
}

type UIBridgeInterface interface {
	NewView(viewName string, VID string)
	ViewSetAttr(VID string, attr string, value string)
	ViewGetAttr(VID string, attr string) string
	Stick2RootView(VID string)
	RunUIThread(fnID string)
	GetPkg() string
	GetCurrentFActivity() *Activity
}

var Gravitys = struct {
	TopLeft, TopCenter, TopRight          string
	LeftCenter, Center, RightCenter       string
	BottomLeft, BottomCenter, BottomRight string
}{
	"TopLeft", "TopCenter", "TopRight",
	"LeftCenter", "Center", "RightCenter",
	"BottomLeft", "BottomCenter", "BottomRight",
}
