package ifaith

//Activity
type Activity struct {
	UI string
}

func (a Activity) GetContext() string {
	return "Activity"
}
func (a *Activity) GetMyActivity() *Activity {
	return a
}

//MainActivity
type MainActivity struct {
	Activity
}

func (a *MainActivity) GetMyActivity() *Activity {
	return &a.Activity
}

//IActivity
type IActivity interface {
	GetMyActivity() *Activity
}

// --------------------------------------------------------

func (a *Activity) SetUIInterface(u UIBridgeInterface) string {
	uId := NewToken()
	GlobalVars.UIs[uId] = u
	a.UI = uId
	return uId
}
func (a *Activity) OnCreate() {

}
func (a *Activity) OnResume() {
}
func (a *Activity) OnPause() {
}
func (a *Activity) OnStart() {
}
func (a *Activity) OnDestroy() {
}
