package ifaith

type FLinearLayout struct {
	FBaseView
	Children  []IView
	showAfter bool
}

func LinearLayout(a IActivity) *FLinearLayout {
	v := &FLinearLayout{}
	v.VID = NewToken()
	v.ClassName = "LinearLayout"
	v.UI = a.GetMyActivity().UI
	GlobalVars.UIs[v.UI].NewView(v.ClassName, v.VID)
	GlobalVars.ViewMap[v.VID] = v
	return v
}

// ----------------------------
func (v *FLinearLayout) Size(w, h int) *FLinearLayout {
	v.FBaseView.Size(w, h)
	return v
}
func (v *FLinearLayout) BackgroundColor(s string) *FLinearLayout {
	v.FBaseView.BackgroundColor(s)
	return v
}
func (v *FLinearLayout) Gravity(s string) *FLinearLayout {
	v.FBaseView.Gravity(s)
	return v
}

// ---------------------------------
func (v *FLinearLayout) Append(vs ...IView) *FLinearLayout {
	v.Children = append(v.Children, vs...)
	for _, i := range vs {
		GlobalVars.UIs[v.UI].ViewSetAttr(v.VID, "AddView", i.GetViewId())
	}
	if v.showAfter {
		v.showAfter = false
		v.Show()
	}
	return v
}
func (v *FLinearLayout) DeferShow() *FLinearLayout {
	v.showAfter = true
	return v
}
