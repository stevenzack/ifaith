//
//  FText.swift
//  IFaithApp
//
//  Created by asd on 2018/10/23.
//  Copyright © 2018 asd. All rights reserved.
//

import UIKit
class FText: FView {
    public var v:UILabel!
    func create(c:UIBridge) -> FView {
        parentBridge = c
        v = UILabel()
        v.numberOfLines = 0
        view = v
        setupView()
        return self
    }
    override func setAttr(attr: String, value: String) {
        
    }
    override func getAttr(attr: String) -> String! {
        return ""
    }
}
