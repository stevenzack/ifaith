//
//  UIBridge.swift
//  IFaithApp
//
//  Created by asd on 2018/10/22.
//  Copyright © 2018 asd. All rights reserved.
//

import UIKit
import Ifaith
class UIBridge:NSObject ,IfaithUIBridgeInterfaceProtocol {
    public var factivity:IfaithActivity!
    public var activity:ViewController!
    public var viewMap = [String:FView]()
    public var rootView:FFrameLayout!
    func create(viewController:ViewController,rootView:UIView,factivity:IfaithActivity!) -> UIBridge {
        self.activity = viewController
        self.rootView = FFrameLayout()
        self.rootView.parentBridge = self
        self.rootView.v = rootView
        self.rootView.view = rootView
        self.factivity = factivity
        self.rootView.className = "FrameLayout"
        self.rootView.vID = "root"
        viewMap["root"] = self.rootView 
        return self
    }
    func newView(_ viewName: String!, vid VID: String!) {
        var v:FView!
        switch viewName {
        case "Button":
            v = FButton().create(c: self)
        case "FrameLayout":
            v = FFrameLayout().create(c:self)
        case "LinearLayout":
            v = FLinearLayout().create(c: self)
        default:
            break
        }
        v.className = viewName
        v.vID = VID
        viewMap[VID] = v
    }
    func viewSetAttr(_ VID: String!, attr: String!, value: String!) {
        if let v = viewMap[VID] {
            v.setAttr(attr: attr, value: value)
        }
    }
    func viewGetAttr(_ VID: String!, attr: String!) -> String! {
        if let v = viewMap[VID] {
            return v.getAttr(attr: attr)
        }
        return ""
    }
    func stick2RootView(_ VID: String!) {
        if let v = viewMap[VID]{
            rootView.v.addSubview(v.view)
            v.execAddedFuncs(rootView)
        }
    }
    func runUIThread(_ fnID: String!) {
        
    }
    func getPkg() -> String! {
        let a = Bundle.main.object(forInfoDictionaryKey: "CFBundleName") as! String
        return a
    }
    func getCurrentFActivity() -> IfaithActivity! {
        return factivity
    }
}
