//
//  Box.swift
//  IFaithApp
//
//  Created by asd on 2018/10/22.
//  Copyright © 2018 asd. All rights reserved.
//

import UIKit

class FLinearLayout: FView {
    public var v:UIStackView!
    func create(c:UIBridge) -> FView {
        parentBridge = c
        v = UIStackView()
        v.clipsToBounds = true
        v.axis = .vertical
        v.alignment = .fill
        v.distribution = .equalSpacing
        v.spacing = 0
        view = v
        setupView()
        return self
    }
    override func getAttr(attr: String) -> String! {
        let str = getUniversalAttr(attr: attr)
        if let s = str{
            return s
        }
        switch attr {
        default:
            return ""
        }
    }
    override func setAttr(attr: String, value: String) {
        if setUniversalAttr(attr: attr, value: value) {
            return
        }
        switch attr {
        case "AddView":
            v.addArrangedSubview(parentBridge.viewMap[value]!.view)
        default:
            break
        }
    }
}
